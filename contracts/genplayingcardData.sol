/**
 *Submitted for verification at Etherscan.io on 2021-08-27
*/

// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/presets/ERC721PresetMinterPauserAutoIdUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/interfaces/IERC2981Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./libs/Helpers/Errors.sol";
import "./libs/Helpers/Base64.sol";
import "./libs/Helpers/NounCard.sol";
import "./interfaces/INounsDescriptorV2.sol";
import "./interfaces/INounsSeeder.sol";
import "./interfaces/IParameterControl.sol";
import "./interfaces/IGenPlayingCardData.sol";
import "./operator-filter-registry/upgradeable/DefaultOperatorFiltererUpgradeable.sol";


contract GenPlayingCardData is Initializable, IGenPlayingCardData {  
    address public _parameterControlAddr;
    address public _nounDescriptorAddr;
    address public _admin;

    uint _fromCard;
    uint _toCard;
    string _name;
    string _description;
    string[4] _propertyNames; 

    mapping  (string => _cardProperty)   _cardProperties;
    string[] _cardNames;
    mapping(string => _cardTitle) _cardTitles;
    string[] private _nounImages;
    mapping(string => string) private _params;
  

    function initialize(
        address admin,
        address paramsAddress,
        address noundescriptorAddress
    ) initializer public {
        require(admin != address(0) && paramsAddress != address(0) && noundescriptorAddress != address(0), Errors.INV_ADD);
       
        _parameterControlAddr = paramsAddress;
        _nounDescriptorAddr = noundescriptorAddress;
        _admin = admin;
       
        _cardNames = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'j', 'q', 'k', 'a'];
        _propertyNames = ['spades', 'hearts', 'diamonds', 'clubs'];
        _fromCard = 8;
        _toCard = _cardNames.length;
    }

    function setNounDescriptorAddr(address _new) external   {
        require(msg.sender == _admin, Errors.ONLY_ADMIN_ALLOWED);
        require(_new != address(0x0),  Errors.INV_ADD);
        _nounDescriptorAddr = _new;
    }
    
    function setCardProperties(string memory hearts, string memory diamons, string memory spades, string memory clubs) external   {
        require(msg.sender == _admin, Errors.ONLY_ADMIN_ALLOWED);
        _propertyNames = [hearts,diamons, spades,clubs];
    }
    
    function setCardTitle(_cardTitle[] memory data) external   {
        require(msg.sender == _admin, Errors.ONLY_ADMIN_ALLOWED);
        for (uint i = 0 ;i < 13; i++){
             _cardTitles[data[i].key] = data[i];
        }
    }
    
    function setCardProperty(_cardProperty[] memory data) external   {
        require(msg.sender == _admin, Errors.ONLY_ADMIN_ALLOWED);
        for (uint i = 0 ;i < 4; i++){
             _cardProperties[data[i].key] = data[i];
        }
    }
    
    function setFromCard(uint fromCard) external   {
        require(msg.sender == _admin, Errors.ONLY_ADMIN_ALLOWED);
        _fromCard = fromCard;
    }

    function setToCard(uint toCard) external   {
        require(msg.sender == _admin, Errors.ONLY_ADMIN_ALLOWED);
        _toCard = toCard;
    }
    
    function changeAdmin(address  _new) external   {
        require(msg.sender == _admin, Errors.ONLY_ADMIN_ALLOWED);
        _admin = _new;
    }
    
    function getAdmin() external  view returns (address)   {
       return _admin;
    }
    
    function createSymbol(string memory id, string memory content) internal view returns (bytes memory){
       return abi.encodePacked('<symbol id = "',id,'">',content,'</symbol>');
    }
     
    function useTag(string memory transform,string memory id ) internal view returns (bytes memory){
        return abi.encodePacked('<use transform="',transform,'"  href="#',id,'" />');
    }
    
    function addCenterImage(bytes memory mainLayout, string memory transform, string memory centerImageKey ) internal view returns (bytes memory){
        return abi.encodePacked('<g transform="',transform,'" stroke="none">',useTag('',centerImageKey),'</g>', mainLayout);
    }

    function getCards(uint256 _tokenId,  string memory cardName) public view returns (bytes memory) {
        bytes memory defs;
        bytes memory uses;
        uint x = 150;
        for (uint j  = 0; j < _propertyNames.length; j ++){
            (bytes memory _def, bytes memory _use) = getCardContent(_tokenId * getPosName(cardName) , cardName, _propertyNames[j], x, 250, 0);
            defs = abi.encodePacked(defs, _def);
            uses = abi.encodePacked(uses, _use);
            x += 220;
        }
        return svgResp(_tokenId, defs, uses);
    }
  
    function getCardContent(uint256 _tokenId,  string memory cardName, string memory cardProperty, uint256 x, uint256 y, uint256 rotate) internal view returns (bytes memory, bytes memory) {
        IParameterControl pram = IParameterControl(_parameterControlAddr);
        (bytes memory _def, bytes memory _use) = generateACard(_tokenId, cardName, cardProperty);
        bytes memory defs = abi.encodePacked(createSymbol(_cardTitles[cardName].name, pram.get(_cardTitles[cardName].name)), _def);
        bytes memory uses = abi.encodePacked('<g transform="translate(',StringsUpgradeable.toString(x),',',StringsUpgradeable.toString(y),') rotate(',StringsUpgradeable.toString(rotate),')" >' ,_use, '</g>');
        return (defs, uses);
    }
    
    function getCard(uint256 _tokenId,  string memory cardName, string memory cardProperty) public view returns (bytes memory) {
        (bytes memory _def, bytes memory _use) = getCardContent(_tokenId * getPosName(cardName), cardName, cardProperty, 50, 45, 0); 
        return svgResp(_tokenId, _def, _use);
    }

    function svgResp(uint256 _tokenId, bytes memory defs, bytes memory uses) internal view returns (bytes memory) {
         return   abi.encodePacked('data:image/svg+xml;base64,',Base64.encode(generateCardLayout(_tokenId, defs, uses, false)));
    }
    
    function htmlResp(uint256 _tokenId, bytes memory defs, bytes memory uses) internal view returns (bytes memory) {
         return   abi.encodePacked('data:text/html;base64,',Base64.encode(generateCardLayout(_tokenId, defs, uses, true)));
    }

    function getNounCards(uint256 _tokenId) external view returns (bytes memory, bytes memory) {
        bytes memory defs;
        bytes memory uses;
        for (uint i=_fromCard;i<_toCard;i++){
            (bytes memory _def, bytes memory _use) = getCardContent(_tokenId * (i+1) , _cardNames[i],  _propertyNames[random(block.timestamp + i, 'property', _propertyNames.length)], 50, 40, 0); 
            defs = abi.encodePacked(defs, _def);
            uses = abi.encodePacked(uses, _use);
        }

       
       return ( svgResp(_tokenId,defs, uses ),  htmlResp(_tokenId,defs, uses ));
    }
    
    function generateCardLayout(uint256 _tokenId, bytes memory defs, bytes memory uses, bool useScript) internal view returns (bytes memory) {
        IParameterControl _p = IParameterControl(_parameterControlAddr);
        string memory style = _p.get('NOUN_BORDER_STYLE');
        bytes memory start =  '<svg id="svg1" xmlns="http://www.w3.org/2000/svg" onload="move()" onresize="move()" preserveAspectRatio="none" width="300" height="350" viewbox="start-x start-y end-x end-y">';
        if (useScript == true) {
            start = abi.encodePacked(start, '<script xmlns="http://www.w3.org/2000/svg" type="text/javascript"> <![CDATA[function move(){',_p.get('CARD_SCRIPT'),'}]]></script>');
        }
        style = string(abi.encodePacked(style,'svg > rect:first-child{fill: none!important;}.RED{fill:',_p.get('RED'),'; stroke:',_p.get('RED'),'}.BLACK{fill:',_p.get('BLACK'),'; stroke:',_p.get('BLACK'),'}'));
        //global
        defs = abi.encodePacked(defs, bytes(createSymbol('border', _p.get('NOUND_BORDER'))), createSymbol(NounCard.NOUN_CARD_NUMBER_MID, _p.get(NounCard.NOUN_CARD_NUMBER_MID)));
        for (uint i = 0; i< _propertyNames.length; i ++) {
            _cardProperty memory property = _cardProperties[_propertyNames[i]];
            string memory iconKey = string.concat(_propertyNames[i],'_icon');
            string memory subIconKey = string.concat(_propertyNames[i],'_sub_icon');
            defs =  abi.encodePacked(defs,createSymbol(iconKey, _p.get(property.icon)), createSymbol(subIconKey, _p.get(property.subIcon)));
        } 
        //end global
        return abi.encodePacked(start,'<defs><style>',style,'</style>',defs,'</defs><g id="cards">',uses,'</g></svg>');
    }

    function generateACard(uint256 _tokenId, string memory _title, string memory _property) internal view returns (bytes memory, bytes memory) { 
        uint256 tokenID = _tokenId;
        IParameterControl pram = IParameterControl(_parameterControlAddr);
        string memory cardID = string.concat(_title,'_',_property);
        _cardTitle memory title = _cardTitles[_title];
        _cardProperty memory property = _cardProperties[_property];
        uint256 head = title.nounHead; 
        if (head == 0) {
            head = getHead(tokenID);
        }
        string memory subIconKey = string.concat(_property,'_sub_icon');
        string memory iconKey = string.concat(_property,'_icon');
        string memory centerImageKey = string.concat(title.name,"_",_property);
        INounsDescriptorV2 _iNDescriptor = INounsDescriptorV2(_nounDescriptorAddr);
        INounsSeeder.Seed memory seed = INounsSeeder.Seed(getBackground(tokenID),getBody(tokenID),getAccessory(tokenID), uint48(head), uint48(property.nounGlasses));
        string memory centerImageSvg = _iNDescriptor.generateSVGImage(seed);
    
        bytes memory def = createSymbol(centerImageKey, string(Base64.decode(centerImageSvg)));
        bytes memory use = abi.encodePacked(useTag(title.titleLeft, title.name), useTag(title.iconPos, iconKey), useTag(title.subIconPos, subIconKey), useTag(title.titleRight, title.name));
        use = abi.encodePacked('<g id="',cardID,'" class="',property.color,'"><use href="#border"/><g>',addCenterImage(use, 'matrix(0.348, 0, 0, 0.348, 39, 11)', centerImageKey),'</g> <g transform="matrix(-1, 0, 0, -1, 186.479996, 254.649994)">',addCenterImage(use, 'matrix(0.348, 0, 0, 0.348, 36.619999, 11)', centerImageKey),'</g><g stroke="none">',useTag('matrix(1, 0, 0, 1, 60, 115.55999755859375)', NounCard.NOUN_CARD_NUMBER_MID),'</g></g>');
        return (def, use);
    }

    function getBackground(uint256 id) internal view returns (uint48) {
        return random(id,'Background', 2);
    }

    function getBody(uint256 id) internal view returns (uint48) {
       return random(id,'Body', 30);
    }

    function getAccessory(uint256 id) internal view returns (uint48) {
        return random(id,'Accessory', 140);
    }

    function getHead(uint256 id) internal view returns (uint48) {
        return  random(id,'Head', 242);
    }

    function getGlasses(uint256 id) internal view returns (uint48) {
        return  random(id,'Glasses', 23);
    }
    
    function getPosName(string memory name) internal view returns (uint) {
        for (uint i=0 ;i < _cardNames.length ; i++){
            string memory _name = _cardNames[i];
            if (keccak256(abi.encodePacked((name))) == keccak256(abi.encodePacked((_name)))){
                return i + 1;
            }
        }
        return 0;
    }

    function random(uint256 id, string memory trait, uint256 length) internal pure returns (uint48) {
        uint256 k = uint256(keccak256(abi.encodePacked(trait, StringsUpgradeable.toString(id))));
        return uint48(k) % uint48(length);
    }
    
}

