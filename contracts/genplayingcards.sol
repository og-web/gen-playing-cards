/**
 *Submitted for verification at Etherscan.io on 2021-08-27
*/

// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/presets/ERC721PresetMinterPauserAutoIdUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/interfaces/IERC2981Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./libs/Helpers/Errors.sol";
import "./libs/Helpers/Base64.sol";
import "./libs/Helpers/NounCard.sol";
import "./interfaces/IGenPlayingCardData.sol";
import "./interfaces/IParameterControl.sol";
import "./operator-filter-registry/upgradeable/DefaultOperatorFiltererUpgradeable.sol";


contract GenPlayingCards is Initializable, ERC721PausableUpgradeable, ReentrancyGuardUpgradeable, OwnableUpgradeable, IERC2981Upgradeable, DefaultOperatorFiltererUpgradeable {  
    address public _admin;
    uint256 public _totalMints;
    address public _genPlayingCardDataAddr;
    string _name;
    string _description;
    uint256 _mintFee;
    
    struct _attribute {
        string traitType;
        string value;
    } 

    _attribute[4] _attributes; 
    function initialize(
        string memory name,
        string memory symbol,
        address admin,
        address paramsAddress,
        address genPlayingCardDataAddr
    ) initializer public {
        require(admin != address(0) && paramsAddress != address(0) && genPlayingCardDataAddr != address(0), Errors.INV_ADD);
        __ERC721_init(name, symbol);
        _genPlayingCardDataAddr = genPlayingCardDataAddr;
        _admin = admin;
        _totalMints = 0;
        _mintFee = 0;
        
        __Ownable_init();
        __ReentrancyGuard_init();
        __ERC721Pausable_init();
    }

    function royaltyInfo(uint256 _tokenId, uint256 _salePrice) external view override
    returns (address receiver, uint256 royaltyAmount)
    {
        receiver = _admin;
        royaltyAmount = (_salePrice * 500) / 10000;
    }
    
    function setGenPlayingCardDataAddr(address _new) external   {
        require(msg.sender == _admin, Errors.ONLY_ADMIN_ALLOWED);
        require(_new != address(0x0),  Errors.INV_ADD);
        _genPlayingCardDataAddr = _new;
    }

    function changeAdmin(address  _new) external   {
        require(msg.sender == _admin, Errors.ONLY_ADMIN_ALLOWED);
        _admin = _new;
    }
    
    function getAdmin() external  view returns (address)   {
       return _admin;
    }
    
    function setName(string memory name) external   {
        require(msg.sender == _admin, Errors.ONLY_ADMIN_ALLOWED);
        _name = name;
    }
    
    function setDesciption(string memory description) external   {
        require(msg.sender == _admin, Errors.ONLY_ADMIN_ALLOWED);
        _description = description;
    } 
    
    function setMintFee(uint256  _new) external   {
        require(msg.sender == _admin, Errors.ONLY_ADMIN_ALLOWED);
        _mintFee = _new;
    }
    
     function getMintFee() external view returns (uint256) {
       return _mintFee;
    }
    
    function setAttributes(_attribute[] memory attrs) external   {
        require(msg.sender == _admin, Errors.ONLY_ADMIN_ALLOWED);
        for (uint i=0; i< attrs.length; i++){
            _attributes[i] = _attribute(attrs[i].traitType, attrs[i].value);
        }
    }

    function tokenURI(uint256 _tokenId) override public view returns (string memory) {
        bytes memory attrs;
        bytes memory suffix = ',';
        for (uint i =0 ; i<_attributes.length; i++) {
            if (i== _attributes.length - 1){
                suffix = '';
            }
            attrs = abi.encodePacked(attrs, '{"trait_type": "',_attributes[i].traitType,'", "value": "',_attributes[i].value,'" }',suffix);
        }
         IGenPlayingCardData _ip = IGenPlayingCardData(_genPlayingCardDataAddr);
        
        (bytes memory svg, bytes memory html) = _ip.getNounCards(_tokenId);
        string memory json = Base64.encode(abi.encodePacked('{"name": "',_name, StringsUpgradeable.toString(_tokenId), '", "description": "',_description,'", "image": "',svg,'","animation_url": "',html,'" , "attributes": [',attrs,'] }'));
        return string(abi.encodePacked('data:application/json;base64,', json)); 
    }

    function mint() external payable  returns (uint256) {
        require(_totalMints < 9000);

        uint256 mintfee = _mintFee;
        require(mintfee == 0 || (mintfee > 0 && msg.value >= mintfee ), Errors.INV_MINTFEE_REQURIED);
        
        _totalMints++;
        _safeMint(msg.sender, _totalMints);
        return _totalMints;
    }

    function ownerMint(uint256 id) public {
        require(msg.sender == _admin, Errors.ONLY_ADMIN_ALLOWED);
        require(id > 9000 && id <= 10000);
        _safeMint(msg.sender, id);
    }

    function withdraw() external nonReentrant {
        require(msg.sender == _admin, Errors.ONLY_ADMIN_ALLOWED);
        (bool success,) = msg.sender.call{value : address(this).balance}("");
        require(success);
    }

    function getCard(uint256 _tokenId,  string memory cardName, string memory cardProperty) public view returns (string memory) {
       IGenPlayingCardData _ip = IGenPlayingCardData(_genPlayingCardDataAddr);
       return  string(_ip.getCard(_tokenId, cardName, cardProperty));
    }
    
     function getCards(uint256 _tokenId,  string memory cardName) public view returns (string memory) {
       IGenPlayingCardData _ip = IGenPlayingCardData(_genPlayingCardDataAddr);
       return  string(_ip.getCards(_tokenId, cardName));
    }
        
    /* @notice: opensea operator filter registry
    */
    function transferFrom(address from, address to, uint256 tokenId) public override onlyAllowedOperator(from) {
        super.transferFrom(from, to, tokenId);
    }

    function safeTransferFrom(address from, address to, uint256 tokenId) public override onlyAllowedOperator(from) {
        super.safeTransferFrom(from, to, tokenId);
    }

    function safeTransferFrom(address from, address to, uint256 tokenId, bytes memory data)
    public
    override
    onlyAllowedOperator(from)
    {
        super.safeTransferFrom(from, to, tokenId, data);
    }

}

