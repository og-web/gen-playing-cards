// SPDX-License-Identifier: MIT
pragma solidity 0.8.12;

interface IGenPlayingCardData {

    struct _cardProperty {
        string key;
        string icon;
        string subIcon;
        string color;
        uint nounGlasses;
    } 

    struct _cardTitle {
        string key;
        string name;
        uint nounHead;
        string titleLeft;
        string titleRight;
        string iconPos;
        string subIconPos;
    } 

   
    function setToCard(uint toCard) external;
    function setFromCard(uint fromCard) external;
    function getCard(uint256 _tokenId,  string memory cardName, string memory cardProperty) external view returns (bytes memory);
    function getCards(uint256 _tokenId,  string memory cardName) external view returns (bytes memory);
    function getNounCards(uint256 _tokenId) external view returns (bytes memory, bytes memory);

}
