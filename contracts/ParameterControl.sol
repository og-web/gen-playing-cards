// SPDX-License-Identifier: MIT
pragma solidity 0.8.12;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "hardhat/console.sol";
import "./interfaces/IParameterControl.sol";
import "./libs/CardAttrs/CardProperties.sol";

/*
 * @dev Implementation of a programmable parameter control.
 *
 * [x] Add (key, value)
 * [x] Add access control
 *
 */

contract ParameterControl is AccessControl, IParameterControl {
    event AdminChanged(address previousAdmin, address newAdmin);
    event Set(string key, string value);

    address public admin; // is a mutil sig address when deploy
    mapping(string => string) private _params;
    mapping(string => int256) private _paramsInt;
    mapping(string => uint256) private _paramsUInt256;
    mapping(string => address) private _paramsAddress;

    constructor(address admin_) {
        require(admin_ != address(0x0), "admin is zero address");
        admin = admin_;
        _setupRole(DEFAULT_ADMIN_ROLE, admin);
        _params["BLACK"] = '#1e1e1e';
        _params["RED"] = '#ce0000';

        _params["NOUND_BORDER"] = CardProperties.NOUN_BORDER;
        _params["NOUN_SPADES"] = CardProperties.NOUN_SPADES;
        _params["NOUN_HEARTS"] = CardProperties.NOUN_HEARTS;
        _params["NOUN_DIAMONDS"] = CardProperties.NOUN_DIAMONDS;
        _params["NOUN_CLUBS"] = CardProperties.NOUN_CLUBS;
        
        _params["NOUN_GLASS_SPADES"] = CardProperties.NOUN_GLASS_SPADES;
        _params["NOUN_GLASS_HEARTS"] = CardProperties.NOUN_GLASS_HEARTS;
        _params["NOUN_GLASS_DIAMONDS"] = CardProperties.NOUN_GLASS_DIAMONDS;
        _params["NOUN_GLASS_CLUBS"] = CardProperties.NOUN_GLASS_CLUBS;
        _params["NOUN_BORDER_STYLE"] = CardProperties.NOUN_BORDER_STYLE;

        _params["NOUN_CARD_TITLE_A"] = CardProperties.NOUN_CARD_TITLE_A;
        _params["NOUN_CARD_TITLE_2"] = CardProperties.NOUN_CARD_TITLE_2;
        _params["NOUN_CARD_TITLE_3"] = CardProperties.NOUN_CARD_TITLE_3;
        _params["NOUN_CARD_TITLE_4"] = CardProperties.NOUN_CARD_TITLE_4;
        _params["NOUN_CARD_TITLE_5"] = CardProperties.NOUN_CARD_TITLE_5;
        _params["NOUN_CARD_TITLE_6"] = CardProperties.NOUN_CARD_TITLE_6;
        _params["NOUN_CARD_TITLE_7"] = CardProperties.NOUN_CARD_TITLE_7;
        _params["NOUN_CARD_TITLE_8"] = CardProperties.NOUN_CARD_TITLE_8;
        _params["NOUN_CARD_TITLE_9"] = CardProperties.NOUN_CARD_TITLE_9;
        _params["NOUN_CARD_TITLE_10"] = CardProperties.NOUN_CARD_TITLE_10;
        _params["NOUN_CARD_TITLE_J"] = CardProperties.NOUN_CARD_TITLE_J;
        _params["NOUN_CARD_TITLE_Q"] = CardProperties.NOUN_CARD_TITLE_Q;
        _params["NOUN_CARD_TITLE_K"] = CardProperties.NOUN_CARD_TITLE_K;
        _params["NOUN_CARD_NUMBER_MID"] = CardProperties.NOUN_CARD_NUMBER_MID;

        
    }

    function get(string memory key)
        external
        view
        override
        returns (string memory)
    {
        return _params[key];
    }

    function getInt(string memory key) external view override returns (int256) {
        return _paramsInt[key];
    }

    function getUInt256(string memory key)
        external
        view
        override
        returns (uint256)
    {
        return _paramsUInt256[key];
    }

    function getAddress(string memory key)
        external
        view
        override
        returns (address)
    {
        return _paramsAddress[key];
    }

    function set(string memory key, string memory value) external override {
        console.log("msg.sender %s", msg.sender);
        require(msg.sender == admin, "Sender is not admin");
        require(
            hasRole(DEFAULT_ADMIN_ROLE, msg.sender),
            "Caller is not a admin"
        );
        _params[key] = value;
        emit Set(key, value);
    }

    function setInt(string memory key, int256 value) external override {
        console.log("msg.sender %s", msg.sender);
        require(msg.sender == admin, "Sender is not admin");
        require(
            hasRole(DEFAULT_ADMIN_ROLE, msg.sender),
            "Caller is not a admin"
        );
        _paramsInt[key] = value;
    }

    function setUInt256(string memory key, uint256 value) external override {
        console.log("msg.sender %s", msg.sender);
        require(msg.sender == admin, "Sender is not admin");
        require(
            hasRole(DEFAULT_ADMIN_ROLE, msg.sender),
            "Caller is not a admin"
        );
        _paramsUInt256[key] = value;
    }

    function setAddress(string memory key, address value) external override {
        console.log("msg.sender %s", msg.sender);
        require(msg.sender == admin, "Sender is not admin");
        require(
            hasRole(DEFAULT_ADMIN_ROLE, msg.sender),
            "Caller is not a admin"
        );
        _paramsAddress[key] = value;
    }

    function updateAdmin(address admin_) external {
        require(msg.sender == admin);
        require(
            hasRole(DEFAULT_ADMIN_ROLE, msg.sender),
            "Caller is not a admin"
        );
        require(admin_ != address(0x0), "admin is zero address");

        console.log("set new admin %s -> %s", admin, admin_);
        address previousAdmin = admin;
        admin = admin_;
        _setupRole(DEFAULT_ADMIN_ROLE, admin);
        _revokeRole(DEFAULT_ADMIN_ROLE, previousAdmin);
        emit AdminChanged(previousAdmin, admin);
    }
}
