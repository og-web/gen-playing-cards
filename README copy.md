# Env file:

- Please update the following values in your .env file
  ```
  NETWORK=rinkeby
  PRIVATE_KEY={your_private_key}
  PUBLIC_KEY={your_public_key}
  ```

# Compile:

    ~~~
    hh:compile
    ~~~

# Deployment:

1. governance/ParameterControl.sol, use the following command:

   ```
    yarn ts:run ./migrations/param/1.1_deploy.ts
   ```

   - It will return the param control address like this

   ```
   Param control deployed: 0x55Df4BB8f42d91Db43AeEF5Ba6dc8C011991d349
   rinkeby Param control deployed address: 0x55Df4BB8f42d91Db43AeEF5Ba6dc8C011991d349
   ```

2. services/Esrow.sol

   - Update the contract_address in **./smart-contract/migrations/services/escrow/1.1_deploy.ts** with the respond address of step 1.

   - Run:

   ```
   yarn ts:run ./migrations/services/escrow/1.1_deploy.ts
   ```

   - The output will be like this:

   ```
   OGEscrow.deploying ...
   OGEscrow deployed at proxy: 0xE817372D388420E62b69f81FF6Dc078FF9b8cf1d
   rinkeby EscrowContract deployed address: 0xE817372D388420E62b69f81FF6Dc078FF9b8cf1d
   ```

3. goods/AutonomousModelsNFT.sol

   - Run:

   ```
   yarn ts:run ./migrations/goods/autonomous_models_NFT/1.1_deploy.ts
   ```

   - The result will be like this:

   ```
       { futureAddress: '0x09553F95858b33B3856cCE6c40A8f5fa4503905e' }
       Network run rinkeby https://rpc.ankr.com/eth_rinkeby
       OGNFT deployed: 0xbfe6B966d3073E84cAfe7FD13bAcDaDeF32370eb
       Model NFT deployed address:  0xbfe6B966d3073E84cAfe7FD13bAcDaDeF32370eb
   ```

   - Please save the Model NFT deployed address: {} in the safe place.

4. set "setApproveNft.whiteListNft" in "services/Esrow.sol" is "goods/AutonomousModelsNFT.sol"

   - Update 2 variable in **./migrations/services/escrow/2.1_setApproveContract.ts**

     - const contract = '{OGEscrow_addres_step_2}';
     - const nftApproveContract = '{OGNFT_addres_step_3}';

   - Run:

     ```
     yarn ts:run ./migrations/services/escrow/2.1_setApproveContract.ts
     ```

5. set "accountingAddress" in "services/Esrow.sol"

   - Update 2 variable in **./migrations/services/escrow/2.2_setAccountingAddress.ts**

     - const contract = '{OGEscrow_addres_step_2}';
     - const accountingAddress = '{Accouting_address}';

   - Run:
     `yarn ts:run ./migrations/services/escrow/2.2_setAccountingAddress.ts`

6. set "CREATE_MODEL_FEE_ERC20" in "param/setErc20"

   - Update 2 variables in **./migrations/param/2.2_setErc20.ts**

     - const contract = '{Your parameter contract address}';
     - const value = '{Your value}';

   - Run:
     `yarn ts:run ./migrations/param/2.2_setErc20.ts`

7. set "CREATE_MODEL_FEE" in "param/setUint256"

   - Update 2 variables in **./migrations/param/2.1_setUint256.ts**

     - const contract = '{Your parameter contract address}';
     - const value = '{Your value}';

   - Run:
     `yarn ts:run ./migrations/param/2.1_setUint256.ts`

8. set "currentBaseUri" in "./migrations/goods/autonomous_models_NFT/4_setbaseuri.ts"

   - Update 1 variables in **./migrations/goods/autonomous_models_NFT/4_setbaseuri.ts**

     - const baseUri = '{Your baseUri}';

     - Your uri has the following format {link to metadata}/{chain id}/{model Nft contract address}

- Run:
  `yarn ts:run ./migrations/goods/autonomous_models_NFT/4_setbaseuri.ts`

# Commands:

- Set parameter:

  ```
  yarn ts:run ./migrations/param/2.1_setUint256.ts
  ```

# Set parameter:
