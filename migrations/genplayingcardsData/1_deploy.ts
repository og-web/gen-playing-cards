import { GENPLAYINGCARDDDATA } from "./GENPLAYINGCARDDDATA";

const { ethers } = require("hardhat");
const { getContractAddress } = require("@ethersproject/address");

(async () => {
  try {
    // if (process.env.NETWORK != "local") {
    //   console.log("wrong network");
    //   return;
    // }

    const nft = new GENPLAYINGCARDDDATA(
      process.env.NETWORK,
      process.env.PRIVATE_KEY,
      process.env.PUBLIC_KEY
    );

    const [owner] = await ethers.getSigners();
    const transactionCount = await owner.getTransactionCount();

    const futureAddress = getContractAddress({
      from: owner.address,
      nonce: transactionCount,
    });
    console.log({ futureAddress });
   
    const address = await nft.deploy();
    console.log("GenPlayingCardsNFT deployed address: ", address);
  } catch (e) {
    // Deal with the fact the chain failed
    console.log(e);
  }
})();
