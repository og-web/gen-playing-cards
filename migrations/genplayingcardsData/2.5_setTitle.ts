import {GENPLAYINGCARDDDATA} from "./GENPLAYINGCARDDDATA";

(async () => {
    try {
        // _cardTitles['a'] = _cardTitle('NOUN_CARD_TITLE_A', 2 ); 
        // _cardTitles['2'] = _cardTitle('NOUN_CARD_TITLE_2', 0);
        // _cardTitles['3'] = _cardTitle('NOUN_CARD_TITLE_3', 0);
        // _cardTitles['4'] = _cardTitle('NOUN_CARD_TITLE_4', 0);
        // _cardTitles['5'] = _cardTitle('NOUN_CARD_TITLE_5', 0);
        // _cardTitles['6'] = _cardTitle('NOUN_CARD_TITLE_6', 0);
        // _cardTitles['7'] = _cardTitle('NOUN_CARD_TITLE_7', 0);
        // _cardTitles['8'] = _cardTitle('NOUN_CARD_TITLE_8', 0);
        // _cardTitles['9'] = _cardTitle('NOUN_CARD_TITLE_9', 0);
        // _cardTitles['10'] = _cardTitle('NOUN_CARD_TITLE_10', 0);
        // _cardTitles['j'] = _cardTitle('NOUN_CARD_TITLE_J', 232);
        // _cardTitles['q'] = _cardTitle('NOUN_CARD_TITLE_Q', 168);
        // _cardTitles['k'] = _cardTitle('NOUN_CARD_TITLE_K', 63);

       
        const data =[   ['a','NOUN_CARD_TITLE_A', 2 ,'matrix(1, 0, 0, 1, 13, 10)', 'matrix(1, 0, 0, 1, 161, 10)', 'matrix(1, 0, 0, 1, 161, 35)', 'matrix(1, 0, 0, 1, 13.5, 35)'],
                        ['2','NOUN_CARD_TITLE_2', 0 ,'matrix(1, 0, 0, 1, 13, 10)', 'matrix(1, 0, 0, 1, 161, 10)', 'matrix(1, 0, 0, 1, 161, 35)', 'matrix(1, 0, 0, 1, 13.5, 35)'],
                        ['3','NOUN_CARD_TITLE_3', 0,'matrix(1, 0, 0, 1, 13, 10)', 'matrix(1, 0, 0, 1, 161, 10)', 'matrix(1, 0, 0, 1, 161, 35)', 'matrix(1, 0, 0, 1, 13.5, 35)'],
                        ['4','NOUN_CARD_TITLE_4', 0 ,'matrix(1, 0, 0, 1, 13, 10)', 'matrix(1, 0, 0, 1, 161, 10)', 'matrix(1, 0, 0, 1, 161, 35)', 'matrix(1, 0, 0, 1, 13.5, 35)'],
                        ['5','NOUN_CARD_TITLE_5', 0 ,'matrix(1, 0, 0, 1, 13, 10)', 'matrix(1, 0, 0, 1, 161, 10)', 'matrix(1, 0, 0, 1, 161, 35)', 'matrix(1, 0, 0, 1, 13.5, 35)'],
                        ['6','NOUN_CARD_TITLE_6', 0 ,'matrix(1, 0, 0, 1, 13, 10)', 'matrix(1, 0, 0, 1, 161, 10)', 'matrix(1, 0, 0, 1, 161, 35)', 'matrix(1, 0, 0, 1, 13.5, 35)'],
                        ['7','NOUN_CARD_TITLE_7', 0,'matrix(1, 0, 0, 1, 13, 10)', 'matrix(1, 0, 0, 1, 161, 10)', 'matrix(1, 0, 0, 1, 161, 35)', 'matrix(1, 0, 0, 1, 13.5, 35)'],
                        ['8','NOUN_CARD_TITLE_8', 0 ,'matrix(1, 0, 0, 1, 13, 10)', 'matrix(1, 0, 0, 1, 161, 10)', 'matrix(1, 0, 0, 1, 161, 35)', 'matrix(1, 0, 0, 1, 13.5, 35)'],
                        ['9','NOUN_CARD_TITLE_9', 0 ,'matrix(1, 0, 0, 1, 13, 10)', 'matrix(1, 0, 0, 1, 161, 10)', 'matrix(1, 0, 0, 1, 161, 35)', 'matrix(1, 0, 0, 1, 13.5, 35)'],
                        ['10','NOUN_CARD_TITLE_10', 0 ,'matrix(1, 0, 0, 1, 13, 10)', 'matrix(1, 0, 0, 1, 146, 10)', 'matrix(1, 0, 0, 1, 161, 35)', 'matrix(1, 0, 0, 1, 13.5, 35)'],
                        ['j','NOUN_CARD_TITLE_J', 232,'matrix(1, 0, 0, 1, 13, 10)', 'matrix(1, 0, 0, 1, 161, 10)', 'matrix(1, 0, 0, 1, 161, 35)', 'matrix(1, 0, 0, 1, 13.5, 35)'],
                        ['q','NOUN_CARD_TITLE_Q', 168,'matrix(1, 0, 0, 1, 13, 10)', 'matrix(1, 0, 0, 1, 161, 10)', 'matrix(1, 0, 0, 1, 161, 35)', 'matrix(1, 0, 0, 1, 13.5, 35)'],
                        ['k','NOUN_CARD_TITLE_K', 63,'matrix(1, 0, 0, 1, 13, 10)', 'matrix(1, 0, 0, 1, 161, 10)', 'matrix(1, 0, 0, 1, 161, 35)', 'matrix(1, 0, 0, 1, 13.5, 35)']
                    ]

        const contract =  process.env.CONTRACT_GENPLAYING_CARD_DATA
        const ogNft = new GENPLAYINGCARDDDATA(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
        const address = await ogNft.setCardTitle(contract, data);

        console.log("escrowContract address: " + address)
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();