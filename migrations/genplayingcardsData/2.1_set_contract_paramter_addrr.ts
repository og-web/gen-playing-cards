import {GENPLAYINGCARDDDATA} from "./GENPLAYINGCARDDDATA";

(async () => {
    try {

        const contract =  process.env.CONTRACT_GENPLAYING_CARD_DATA
        const ogNft = new GENPLAYINGCARDDDATA(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
        const address = await ogNft.setContractPrameterAddress(contract);

        console.log("escrowContract address: " + address)
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();