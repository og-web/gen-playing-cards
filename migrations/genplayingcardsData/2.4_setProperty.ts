import {GENPLAYINGCARDDDATA} from "./GENPLAYINGCARDDDATA";

(async () => {
    try {
        // _cardProperties['spades'] = _cardProperty( 'NOUN_SPADES', 'NOUN_GLASS_SPADES', 'BLACK',  3);
        // _cardProperties['hearts'] = _cardProperty( 'NOUN_HEARTS', 'NOUN_GLASS_HEARTS','RED', 15);
        // _cardProperties['diamonds'] = _cardProperty( 'NOUN_DIAMONDS', 'NOUN_GLASS_DIAMONDS','RED', 0);
        // _cardProperties['clubs'] = _cardProperty( 'NOUN_CLUBS', 'NOUN_GLASS_CLUBS','BLACK', 2);

        
        const data =  [
           ['spades', 'NOUN_SPADES', 'NOUN_GLASS_SPADES', 'BLACK', 3],
           ['hearts',  'NOUN_HEARTS', 'NOUN_HEARTS', 'RED', 15],
           ['diamonds', 'NOUN_DIAMONDS', 'NOUN_GLASS_DIAMONDS', 'RED', 0],
           ['clubs','NOUN_CLUBS', 'NOUN_GLASS_CLUBS', 'BLACK', 2],
        ] ;
      
        const contract =  process.env.CONTRACT_GENPLAYING_CARD_DATA
        const ogNft = new GENPLAYINGCARDDDATA(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
        const address = await ogNft.setProperty(contract, data);

        console.log("escrowContract address: " + address)
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();