import {GENPLAYINGCARDSNFT} from "./GENPLAYINGCARDDDATA";

(async () => {
    try {

        const contract =  process.env.CONTRACT
        const ogNft = new GENPLAYINGCARDSNFT(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
        const address = await ogNft.setNounDescriptorAddr(contract);

        console.log("escrowContract address: " + address)
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();