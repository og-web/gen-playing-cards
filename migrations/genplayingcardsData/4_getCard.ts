import {GENPLAYINGCARDDDATA} from "./GENPLAYINGCARDDDATA";

(async () => {
    try {
        //0x5FbDB2315678afecb367f032d93F642f64180aa3 - local
        const contractAddr = process.env.CONTRACT_GENPLAYING_CARD_DATA
        const cardNumber = "10"
        const tokenURI = 3
        //const cardProperty = "hearts"
        const cardProperty = "diamonds"
        //const cardProperty = "clubs"
        const ogNft = new GENPLAYINGCARDDDATA(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);

        const resp = await ogNft.getCard(contractAddr,tokenURI, cardNumber, cardProperty);

        console.log( resp)
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();