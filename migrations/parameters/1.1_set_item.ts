const fs = require('fs');
const { parse } = require('svg-parser');
const { optimize } = require('svgo');
import {ParamControl} from "./paramControl";
import {ethers} from "ethers";


(async () => {
  try {


    var dirname = '/Users/autonomous/go/src/og-web/gen-playing-cards/elements';
    var filename = 'NOUNS-CARD-FA-ROVE.svg';
    var key = 'CARD_BACK';


    fs.readFile(dirname + "/" + filename, 'utf-8', async function(err: any, content: any) {
      if (err) {
        console.log("forEach", err)
        return;
      }

     
      const nft = new ParamControl(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
 
     
      const contract = process.env.CONTRACT_PARAM;
      const value = content;


      let transactionAddress = await nft.set(contract, key, value, 0);
      console.log("%s ParamControl admin address: %s", process.env.NETWORK, transactionAddress);
    
    
      // const val: any = await nft.get(contract, key);
      // console.log("val", val);
    })
    
  
  
  } catch (e) {
    // Deal with the fact the chain failed
    console.log(e);
  }
})();
