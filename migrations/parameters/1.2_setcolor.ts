const fs = require('fs');
const { parse } = require('svg-parser');
const { optimize } = require('svgo');
import {ParamControl} from "./paramControl";
import {ethers} from "ethers";


(async () => {
  try {
   

    const contract = process.env.CONTRACT_PARAM;
    let key = "BLACK";
    let value = "#1e1e1e";

    const nft = new ParamControl(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
    let transactionAddress = await nft.set(contract, key, value, 0);
    console.log("%s ParamControl admin address: %s", process.env.NETWORK, transactionAddress);

   
    key = "RED";
    value = "#ce0000";

    transactionAddress = await nft.set(contract, key, value, 0);
    console.log("%s ParamControl admin address: %s", process.env.NETWORK, transactionAddress);


  } catch (e) {
    // Deal with the fact the chain failed
    console.log(e);
  }
})();
