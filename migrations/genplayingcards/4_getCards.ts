import {GENPLAYINGCARDSNFT} from "./GENPLAYINGCARDSNFT";

(async () => {
    try {
        //0x5FbDB2315678afecb367f032d93F642f64180aa3 - local
        const contractAddr = process.env.CONTRACT
        const cardNumber = "k"
        const tokenURI = 3
        const ogNft = new GENPLAYINGCARDSNFT(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);

        const resp = await ogNft.getCards(contractAddr,tokenURI, cardNumber);

        console.log( resp)
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();