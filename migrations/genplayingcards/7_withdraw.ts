import {GENPLAYINGCARDSNFT} from "./GENPLAYINGCARDSNFT";

(async () => {
    try {
        //0x5FbDB2315678afecb367f032d93F642f64180aa3 - local
        const contractAddr = process.env.CONTRACT
        const ogNft = new GENPLAYINGCARDSNFT(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
        console.log(contractAddr)
        const resp = await ogNft.withdraw(contractAddr);
        console.log(resp);
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();