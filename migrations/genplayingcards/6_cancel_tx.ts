import {GENPLAYINGCARDSNFT} from "./GENPLAYINGCARDSNFT";

(async () => {
    try {
        //0x5FbDB2315678afecb367f032d93F642f64180aa3 - local
        const contractAddr = process.env.CONTRACT
        const transactionTx = '0x11611f75ea3976b108e675eafda08873527b71aefddb27e6c98ef01f2cb71918';
        const ogNft = new GENPLAYINGCARDSNFT(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
        console.log(contractAddr)
        const resp = await ogNft.cancelTx(transactionTx);
        console.log(resp);
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();