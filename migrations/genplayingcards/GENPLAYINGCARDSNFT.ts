// ethereum/scripts/deploy.js
import { createAlchemyWeb3 } from "@alch/alchemy-web3";
import * as path from "path";

const { ethers, upgrades } = require("hardhat");
const hardhatConfig = require("../../hardhat.config");

class GENPLAYINGCARDSNFT {
  network: string;
  senderPublicKey: string;
  senderPrivateKey: string;

  constructor(network: any, senderPrivateKey: any, senderPublicKey: any) {
    this.network = network;
    this.senderPrivateKey = senderPrivateKey;
    this.senderPublicKey = senderPublicKey;
  }

  getContract(contractAddress: any) {
    console.log(
      "Network run",
      this.network,
      hardhatConfig.networks[this.network].url
    );
    
    let API_URL: any;
    API_URL = hardhatConfig.networks[hardhatConfig.defaultNetwork].url;

    // load contract
    let contract = require(path.resolve("./artifacts/contracts/genplayingcards.sol/GenPlayingCards.json"));
    const web3 = createAlchemyWeb3(API_URL);
    const nftContract = new web3.eth.Contract(contract.abi, contractAddress);
    return { web3, nftContract };
  }

  async signedAndSendTx(web3: any, tx: any) {
    const signedTx = await web3.eth.accounts.signTransaction(
      tx,
      this.senderPrivateKey
    );
    if (signedTx.rawTransaction != null) {
      let sentTx = await web3.eth.sendSignedTransaction(
        signedTx.rawTransaction,
        function (err: any, hash: any) {
          if (!err) {
            console.log(
              "The hash of your transaction is: ",
              hash,
              "\nCheck Alchemy's Mempool to view the status of your transaction!"
            );
          } else {
            console.log(
              "Something went wrong when submitting your transaction:",
              err
            );
          }
        }
      );
      return sentTx;
    }
    return null;
  }

  async deploy() {
   
    console.log(
      "Network run",
      this.network,
      hardhatConfig.networks[this.network].url
    );

    const _admin = process.env.PUBLIC_KEY
    const _contractName = process.env.CONTRACT_NAME
    const _contractSymbol = process.env.CONTRACT_SYMBOL
    const _parameterContract = process.env.CONTRACT_PARAM
    const _genPlayingCardData = process.env.CONTRACT_GENPLAYING_CARD_DATA
    
    console.log("_admin", _admin)
    console.log("_contractName", _contractName)
    console.log("_contractSymbol", _contractSymbol)
    console.log("_parameterContract", _parameterContract)
    console.log("_genPlayingCardData", _genPlayingCardData)

    const GenPlayingCardsNFT = await ethers.getContractFactory("GenPlayingCards");
   
    const proxy = await upgrades.deployProxy(
      GenPlayingCardsNFT,
      [
        _contractName,
        _contractSymbol,
        _admin,
        _parameterContract,
        _genPlayingCardData,
      ],
      {
        initializer:
          "initialize(string, string, address, address, address)",
      }
    );
    await proxy.deployed();

    console.log("GenPlayingCardsNFT deployed:", proxy.address);
    return proxy.address;
  }

  async upgradeContract(proxyAddress: any) {
    const contractUpdated = await ethers.getContractFactory("GenPlayingCards");
    upgrades.up;
    console.log("Upgrading CARDS_NFT... by proxy " + proxyAddress);
    const tx = await upgrades.upgradeProxy(proxyAddress, contractUpdated);
    console.log("CARDS_NFT upgraded on tx address " + tx.address);
    return tx;
  }
  
  async getNounSvg(contractAddress: any, cardNumber: any, cardProperty: any, centerImage: any) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce

    //the transaction
    const tx = {
        from: this.senderPublicKey,
        to: contractAddress,
        nonce: nonce,
    }

    const resp: any = await temp?.nftContract.methods.getNounSvg(1).call(tx);
    return resp
  }
  

  async tokenURI(contractAddress: any, tokenURI: any) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce

    //the transaction
    const tx = {
        from: this.senderPublicKey,
        to: contractAddress,
        nonce: nonce,
    }

    const resp: any = await temp?.nftContract.methods.tokenURI(tokenURI).call(tx);
    return resp
  }

  async getCards(contractAddress: any, tokenURI: any, name: any) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce

    //the transaction
    const tx = {
        from: this.senderPublicKey,
        to: contractAddress,
        nonce: nonce,
    }

    const resp: any = await temp?.nftContract.methods.getCards(tokenURI,name).call(tx);
    return resp
  }
  
  async getCard(contractAddress: any, tokenURI: any, name: any,  property: any) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce

    //the transaction
    const tx = {
        from: this.senderPublicKey,
        to: contractAddress,
        nonce: nonce,
    }

    const resp: any = await temp?.nftContract.methods.getCard(tokenURI,name, property).call(tx);
    return resp
  }

  async mint(contractAddress: any, fee: any) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce

    //the transaction
    

    const fun: any = await temp?.nftContract.methods.mint()

    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: 0,
      data: fun.encodeABI(),
     // value: fee
  }

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }
  
  async setContractPrameterAddress(contractAddress: any) {
    
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce
    const paramAddr = process.env.CONTRACT_PARAM;

    console.log(paramAddr)

    //the transaction
    const fun: any =  temp?.nftContract.methods.setCPAddress(paramAddr)

    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: 0,
      data: fun.encodeABI(),
  }

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }
  
  async setNounDescriptorAddr(contractAddress: any) {
    
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce
    const paramAddr = process.env.CONTRACT_NOUN;

    console.log(paramAddr)

    //the transaction
    const fun: any =  temp?.nftContract.methods.setNounDescriptorAddr(paramAddr)

    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: 0,
      data: fun.encodeABI(),
  }

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }
  
  async setProperties(contractAddress: any) {
    
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce
    //the transaction
    const fun: any =  temp?.nftContract.methods.setCardProperties('hearts','diamonds','spades', 'clubs')

    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: 0,
      data: fun.encodeABI(),
  }

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }
  
  async setProperty(contractAddress: any, data: any) {
    
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce
    //the transaction
    console.log(data);
    const fun: any =  temp?.nftContract.methods.setCardProperty(data)

    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: 0,
      data: fun.encodeABI(),
  }

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }
  
  async setCardTitle(contractAddress: any, data: any) {
    
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce
    //the transaction
    console.log(data)
    const fun: any =  temp?.nftContract.methods.setCardTitle(data)

    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: 0,
      data: fun.encodeABI(),
  }

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }
  
  async setFromCard(contractAddress: any, value: any) {
    
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce
    //the transaction
    const fun: any =  temp?.nftContract.methods.setFromCard(value)

    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: 0,
      data: fun.encodeABI(),
  }

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }
  
  async setToCard(contractAddress: any, value: any) {
    
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce
    //the transaction
    const fun: any =  temp?.nftContract.methods.setToCard(value)

    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: 0,
      data: fun.encodeABI(),
  }

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }
  
  async setName(contractAddress: any, value: any) {
    
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce
    //the transaction
    const fun: any =  temp?.nftContract.methods.setName(value)

    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: 0,
      data: fun.encodeABI(),
  }

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }

  async setDesciption(contractAddress: any, value: any) {
    
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce
    //the transaction
    const fun: any =  temp?.nftContract.methods.setDesciption(value)

    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: 0,
      data: fun.encodeABI(),
  }

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }
  
  async setAttributes(contractAddress: any, value: any) {
    
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce
    //the transaction
    const fun: any =  temp?.nftContract.methods.setAttributes(value)

    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: 0,
      data: fun.encodeABI(),
  }

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }

  async cancelTx(transactionTx: any) {
    let API_URL: any;
    API_URL = hardhatConfig.networks[hardhatConfig.defaultNetwork].url;
    // load contract
    const web3 = createAlchemyWeb3(API_URL)
    var accountOneGasPrice = (await web3.eth.getTransaction(transactionTx));
    console.log(API_URL);
    console.log(transactionTx);
    console.log(accountOneGasPrice);
    //the transaction
    const tx = {
        from: this.senderPublicKey,
        to: "0xF61234046A18b07Bf1486823369B22eFd2C4507F",
        nonce: accountOneGasPrice ? accountOneGasPrice["nonce"] : 0,
        gas: accountOneGasPrice ? accountOneGasPrice["gas"] : 0,
        value: 0,
    }

    return await this.signedAndSendTx(web3, tx);
  }
  
  async withdraw(contractAddress: any) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce

    //the transaction
    

    const fun: any = await temp?.nftContract.methods.withdraw()

    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: 0,
      data: fun.encodeABI(),
     // value: fee
    }

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }

}

export { GENPLAYINGCARDSNFT };
