import {GENPLAYINGCARDSNFT} from "./GENPLAYINGCARDSNFT";
import {ethers} from "ethers";

(async () => {
    try {
        //0x5FbDB2315678afecb367f032d93F642f64180aa3 - local
        const contractAddr = process.env.CONTRACT
        //const cardProperty = "hearts"
        //const cardProperty = "diamonds"
        //const cardProperty = "clubs"
        const ogNft = new GENPLAYINGCARDSNFT(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);

        const resp = await ogNft.mint(contractAddr, ethers.utils.parseEther("0.001"));

        console.log("resp: " + resp)
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();