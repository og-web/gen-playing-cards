import {GENPLAYINGCARDSNFT} from "./GENPLAYINGCARDSNFT";

(async () => {
    try {
        //0x5FbDB2315678afecb367f032d93F642f64180aa3 - local
        const contractAddr = process.env.CONTRACT
        const tokenURI = 2
        const ogNft = new GENPLAYINGCARDSNFT(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
        console.log(contractAddr)
        const from = 8;
        const to = 13;
        const resp = await ogNft.tokenURI(contractAddr, tokenURI);
        console.log(resp);
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();