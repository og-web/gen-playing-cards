import {GENPLAYINGCARDSNFT} from "./GENPLAYINGCARDSNFT";

(async () => {
    try {
        const contract =  process.env.CONTRACT
        const ogNft = new GENPLAYINGCARDSNFT(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
        const address = await ogNft.setName(contract, 'Genplaying cards #');

        console.log("From card is set at address: " + address)
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();