import {GENPLAYINGCARDSNFT} from "./GENPLAYINGCARDSNFT";

(async () => {
    try {
        const attrs = [
            ['Collection', 'BAYC'],
            ['Item', 'Coaster'],
            ['Parent', 'BAYC #4848'],
            ['Type', 'Tableware'],
        ]
        const contract =  process.env.CONTRACT
        const ogNft = new GENPLAYINGCARDSNFT(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
        const address = await ogNft.setAttributes(contract, attrs);

        console.log("From card is set at address: " + address)
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();