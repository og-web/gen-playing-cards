import * as dotenv from 'dotenv';

import {NOUN} from "./noun";

(async () => {
    try {
        const nft = new NOUN(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);
        const address = await nft.deploy(process.env.PUBLIC_KEY);
        console.log("%s Noun contract deployed address: %s", process.env.NETWORK, address);
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();