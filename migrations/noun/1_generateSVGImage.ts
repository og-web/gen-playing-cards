import {NOUN} from "./noun";

(async () => {
    try {
        //0x5FbDB2315678afecb367f032d93F642f64180aa3 - local
        const contractAddr = process.env.CONTRACT_NOUN
        //const cardProperty = "hearts"
        //const cardProperty = "diamonds"
        //const cardProperty = "clubs"
        const ogNft = new NOUN(process.env.NETWORK, process.env.PRIVATE_KEY, process.env.PUBLIC_KEY);

        const resp = await ogNft.generateSVGImage(contractAddr);

        console.log("resp: " + resp)
    } catch (e) {
        // Deal with the fact the chain failed
        console.log(e);
    }
})();