// ethereum/scripts/deploy.js
import { createAlchemyWeb3 } from "@alch/alchemy-web3";
import * as path from "path";

const { ethers, upgrades } = require("hardhat");
const hardhatConfig = require("../../hardhat.config");

class NOUN {
  network: string;
  senderPublicKey: string;
  senderPrivateKey: string;

  constructor(network: any, senderPrivateKey: any, senderPublicKey: any) {
    this.network = network;
    this.senderPrivateKey = senderPrivateKey;
    this.senderPublicKey = senderPublicKey;
  }

  async deploy(adminAddress: any) {
      console.log("adminAddress", adminAddress);
      console.log("Network run", this.network, hardhatConfig.networks[this.network].url);
    
      const param = await ethers.getContractFactory("NounContractV2");
      // const EnvironmentNFTDeploy = await EnvironmentNFT.deploy(adminAddress, operatorAddress, {maxFeePerGas: ethers.utils.parseUnits("28.0", "gwei")});
      const paramDeployed = await param.deploy();

      console.log("Noun control deployed:", paramDeployed.address);
      return paramDeployed.address;
  }

  getContract(contractAddress: any) {
    console.log("Network run", this.network, hardhatConfig.networks[this.network].url);
   
    let API_URL: any;
    API_URL = hardhatConfig.networks[hardhatConfig.defaultNetwork].url;

    // load contract
    let contract = require(path.resolve("./artifacts/contracts/NounContractV2.sol/NounContractV2.json"));
    const web3 = createAlchemyWeb3(API_URL)
    const nftContract = new web3.eth.Contract(contract.abi, contractAddress)
    return {web3, nftContract};
  }

  async signedAndSendTx(web3: any, tx: any) {
      const signedTx = await web3.eth.accounts.signTransaction(tx, this.senderPrivateKey)
      if (signedTx.rawTransaction != null) {
          let sentTx = await web3.eth.sendSignedTransaction(
              signedTx.rawTransaction,
              function (err: any, hash: any) {
                  if (!err) {
                      console.log(
                          "The hash of your transaction is: ",
                          hash,
                          "\nCheck Alchemy's Mempool to view the status of your transaction!"
                      )
                  } else {
                      console.log(
                          err
                      )
                  }
              }
          )
          return sentTx;
      }
      return null;
  }

  async upgradeContract(proxyAddress: any) {
    const contractUpdated = await ethers.getContractFactory("GenPlayingCards");
    upgrades.up;
    console.log("Upgrading CARDS_NFT... by proxy " + proxyAddress);
    const tx = await upgrades.upgradeProxy(proxyAddress, contractUpdated);
    console.log("CARDS_NFT upgraded on tx address " + tx.address);
    return tx;
  }
  
  async getNounSvg(contractAddress: any, cardNumber: any, cardProperty: any, centerImage: any) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce

    //the transaction
    const tx = {
        from: this.senderPublicKey,
        to: contractAddress,
        nonce: nonce,
    }

    const resp: any = await temp?.nftContract.methods.getNounSvg(cardNumber, cardProperty, centerImage).call(tx);
    return resp
  }
  

  async tokenURI(contractAddress: any, tokenURI: any) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce

    //the transaction
    const tx = {
        from: this.senderPublicKey,
        to: contractAddress,
        nonce: nonce,
    }

    const resp: any = await temp?.nftContract.methods.tokenURI(tokenURI, [1,2,3,4,5]).call(tx);
    return resp
  }
  
  async generateSVGImage(contractAddress: any) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce

    //the transaction
    const tx = {
        from: this.senderPublicKey,
        to: contractAddress,
        nonce: nonce,
    }

    const resp: any = await temp?.nftContract.methods.generateSVGImage([1,2,3,4,5]).call(tx);
    return resp
  }

  async mint(contractAddress: any) {
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce

    //the transaction
    

    const fun: any = await temp?.nftContract.methods.mint()

    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: 0,
      data: fun.encodeABI(),
     // value: fee
  }

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }
  
  async setContractPrameterAddress(contractAddress: any) {
    
    let temp = this.getContract(contractAddress);
    const nonce = await temp?.web3.eth.getTransactionCount(this.senderPublicKey, "latest") //get latest nonce
    const paramAddr = process.env.CONTRACT_PARAM;

    console.log(paramAddr)

    //the transaction
    const fun: any =  temp?.nftContract.methods.setCPAddress(paramAddr)

    const tx = {
      from: this.senderPublicKey,
      to: contractAddress,
      nonce: nonce,
      gas: 0,
      data: fun.encodeABI(),
  }

    if (tx.gas == 0) {
      tx.gas = await fun.estimateGas(tx);
    }

    return await this.signedAndSendTx(temp?.web3, tx);
  }

}

export { NOUN };
