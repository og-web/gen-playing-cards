start:
	- yarn hh:network

size:
	- yarn hh:size

compile:
	- yarn hh:compile

deploy_param:
	- yarn ts:run ./migrations/parameters/1.0_deploy_parameters.ts

deploy_noun:
	- yarn ts:run ./migrations/noun/0_deploy_noun.ts

set_param:
	- yarn ts:run ./migrations/parameters/1.1_set_item.ts

set_param_color:
	- yarn ts:run ./migrations/parameters/1.2_setcolor.ts

deploy:
	- yarn ts:run ./migrations/genplayingcards/1_deploy.ts

upgrade:
	- yarn ts:run ./migrations/genplayingcards/2_upgradeContract.ts
	
upgrade_contract_pra_addr:
	- yarn ts:run ./migrations/genplayingcards/2.1_set_contract_paramter_addrr.ts

upgrade_noun_addr:
	- yarn ts:run ./migrations/genplayingcards/2.2_set_noun_contract.ts
	
mint:
	- yarn ts:run ./migrations/genplayingcards/3_mint.ts

card:
	- yarn ts:run ./migrations/genplayingcards/4_getCard.ts

cards:
	- yarn ts:run ./migrations/genplayingcards/4_getCards.ts

token_uri:
	- yarn ts:run ./migrations/genplayingcards/5_getTokenURI.ts
	
test_get_noun:
	- yarn ts:run ./migrations/nouns/3.0_get_nouns.ts

upgrade_run:
	- make compile
	- make upgrade
	- make svg